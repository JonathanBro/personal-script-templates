namespace Personal.ScriptTemplate
{
    using UnityEditor;

    /// <summary>
    /// Custom script templates
    /// </summary>
    internal class ScriptTemplates
    {
        public const string TemplatesRoot = "Packages/com.jonathanbro.personal.scripttemplate/Editor";

        //======================================================
        // C#
        //======================================================
        [MenuItem("Assets/Create/ScriptTemplate/EmptyClass")]
        public static void CreateEmptyClass()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(
                $"{TemplatesRoot}/EmptyClass.txt",
                "NewClass.cs");
        }

        [MenuItem("Assets/Create/ScriptTemplate/EmptyInterface")]
        public static void CreateEmptyInterface()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(
                $"{TemplatesRoot}/EmptyInterface.txt",
                "NewInterface.cs");
        }

        //======================================================
        // Unity Core
        //======================================================
        [MenuItem("Assets/Create/ScriptTemplate/MonoBehaviour")]
        public static void CreateMonoBehaviourClass()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(
                $"{TemplatesRoot}/MonoBehaviour.txt",
                "NewMonoBehaviour.cs");
        }

        //======================================================
        // ECS
        //======================================================
        [MenuItem("Assets/Create/ScriptTemplate/ECS/Component")]
        public static void CreateEcsComponent()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(
                $"{TemplatesRoot}/EcsComponentStruct.txt",
                "NewComponent.cs");
        }

        [MenuItem("Assets/Create/ScriptTemplate/ECS/SystemJobForEach")]
        public static void CreateEcsSystemJobForEach()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(
                $"{TemplatesRoot}/EcsSystemJobForEach.txt",
                "NewSystemJobForEach.cs");
        }

        [MenuItem("Assets/Create/ScriptTemplate/ECS/SystemJobParallelFor")]
        public static void CreateEcsSystemJobParallelForEach()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(
                $"{TemplatesRoot}/EcsSystemJobParallellFor.txt",
                "NewSystemJobParallelFor.cs");
        }

        [MenuItem("Assets/Create/ScriptTemplate/ECS/SystemGroup")]
        public static void CreateEcsSystemGroup()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(
                $"{TemplatesRoot}/EcsSystemGroup.txt",
                "NewSystemGroup.cs");
        }
    }
}